package web

import (
	"fmt"
	"html/template"
	"regexp"
	"strings"

	"../core"
	"../plugins"
	"github.com/gin-gonic/gin"
	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/parser"
)

// Plugins - current wiki plugins
var Plugins []plugins.GokuPlugin

// PluginsPre - plugins run before rendering markdown
var PluginsPre []plugins.GokuPlugin

// InitPlugins - init plugins used in wiki
func InitPlugins(engine *gin.Engine) {
	Plugins = []plugins.GokuPlugin{}
	PluginsPre = []plugins.GokuPlugin{}
	Plugins = append(Plugins, plugins.PluginAddPageCreate())
	Plugins = append(Plugins, plugins.PluginPagelistCreate())
	Plugins = append(Plugins, plugins.PluginMedialistCreate())
	Plugins = append(Plugins, plugins.PluginThumbnailCreate())
	Plugins = append(Plugins, plugins.PluginHighlightCreate())
	Plugins = append(Plugins, plugins.PluginGalleryCreate())
	for _, pl := range Plugins {
		pl.RoutesFunc(&pl, engine)
	}
	PluginsPre = append(PluginsPre, plugins.PluginIncludeCreate())
	PluginsPre = append(PluginsPre, plugins.PluginPopoverCreate())
	PluginsPre = append(PluginsPre, plugins.PluginFoldCreate())
}

// PrepareConfigStyle - prepare style based on config values
func PrepareConfigStyle() string {
	return fmt.Sprintf("<style>body { font-size: %v px;}</style>", core.AppConfig.Globals.BaseFontSize)
}

// PreparePagePath - prepare links for page path
func PreparePagePath(path string) map[string]string {
	parts := strings.Split(path, ":")
	result := map[string]string{}
	for i, part := range parts {
		key := strings.Join(parts[:i+1], ":")
		result[key] = part
	}
	return result
}

// RenderPage - render page template.
func RenderPage(path string) (string, map[string]interface{}) {
	text := core.GetRawPageData(path)
	for _, plugin := range PluginsPre {
		text = plugin.RenderFunc(&plugin, text, core.Ns(path))
	}
	text, toc := RenderMarkdownString(text)
	cw := "col-md-12 col-lg-12"
	sw := "hidden"
	if core.HasSidebar() {
		cw = "col-md-8 col-lg-8"
		sw = "col-md-4 col-lg-4"
	}
	for _, plugin := range Plugins {
		text = plugin.RenderFunc(&plugin, text, core.Ns(path))
	}
	return "page.html", gin.H{
		"content":       template.HTML(text),
		"content_width": cw, "sidebar_width": sw,
		"page_path":      PreparePagePath(path),
		"pagename":       path,
		"base_font_size": core.AppConfig.Globals.BaseFontSize,
		"toc":            toc,
		"hastoc":         len(toc) > 0,
		"showbar":        true}
}

// RenderPageString - render page from string
func RenderPageString(text string) (string, map[string]interface{}) {
	txt := text
	for _, plugin := range PluginsPre {
		txt = plugin.RenderFunc(&plugin, txt, "")
	}
	txt, toc := RenderMarkdownString(txt)
	cw := "col-md-12 col-lg-12"
	sw := "hidden"
	if core.HasSidebar() {
		cw = "col-md-8 col-lg-8"
		sw = "col-md-4 col-lg-4"
	}
	for _, plugin := range Plugins {
		txt = plugin.RenderFunc(&plugin, txt, "")
	}
	return "page.html", gin.H{
		"content":       template.HTML(txt),
		"content_width": cw, "sidebar_width": sw,
		"page_path":      map[string]string{},
		"pagename":       "",
		"base_font_size": core.AppConfig.Globals.BaseFontSize,
		"toc":            toc,
		"showbar":        false}
}

// RenderPageDelete - render page deletion page
func RenderPageDelete(path string) (string, map[string]interface{}) {
	return "pagedel.html", gin.H{"page_path": path}
}

// AdjustLinks - adjust links to pages
func AdjustLinks(text string) string {
	content := text
	reg := regexp.MustCompile(`\[(.*)\]\((.*)\)`)
	indexes := reg.FindAllStringSubmatchIndex(content, -1)
	adjustment := 0
	for _, j := range indexes { //i is the index, j is the element (in this case j = []int )
		page := content[j[4]+adjustment : j[5]+adjustment]
		replacement := "/page/" + page
		switch {
		case strings.HasPrefix(page, "media://"):
			replacement = "/media/" + strings.ReplaceAll(page, "media://", "")
		case len(content) > j[0]+adjustment && j[0]+adjustment > 0 && string(content[j[0]+adjustment-1]) == "!":
			replacement = "/image/" + page
		}
		content = content[:j[4]+adjustment] + replacement + content[j[5]+adjustment:]
		adjustment += len(replacement) - len(page)
	}
	return content
}

// TocInfo - table of contents entry.
type TocInfo struct {
	Name  string
	Path  string
	Level string
}

// PrepareToc - prepare table of contents
func PrepareToc(content string) []TocInfo {
	toc := []TocInfo{}
	reg := regexp.MustCompile(`(?m)^\s*([#]+)\s+(.*)$`)
	indexes := reg.FindAllStringSubmatchIndex(content, -1)
	for _, j := range indexes {
		header := content[j[4]:j[5]]
		level := content[j[2]:j[3]]
		toc = append(toc, TocInfo{header,
			strings.ReplaceAll(strings.ToLower(header), " ", "-"),
			strings.ReplaceAll(level, "#", ". ")})
	}
	return toc
}

const CreateForm = "<form method=\"POST\" action=\"/create\"><input type=\"hidden\" name=\"pagepath\" value=\"%v\"/><button class=\"btn btn-primary\" type=\"submit\">Create</button></form>"

// RenderMarkdown - load text from page file
func RenderMarkdown(pagepath string) (string, []TocInfo) {
	dat := core.GetRawPageData(pagepath)
	// if dat == "" {
	// 	form := fmt.Sprintf(CreateForm, pagepath)
	// 	return fmt.Sprintf("<span class=\"error\">Error rendering page %v</span>. %v", pagepath, form), []TocInfo{}
	// }
	dat = AdjustLinks(dat)
	toc := PrepareToc(dat)
	extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.NoEmptyLineBeforeBlock | parser.DefinitionLists | parser.Includes | parser.Mmark
	parser := parser.NewWithExtensions(extensions)
	output := markdown.ToHTML([]byte(dat), parser, nil)
	return string(output), toc
}

// RenderMarkdownString - render markdown from string
func RenderMarkdownString(text string) (string, []TocInfo) {
	dat := AdjustLinks(text)
	toc := PrepareToc(dat)
	extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.NoEmptyLineBeforeBlock | parser.DefinitionLists | parser.Includes | parser.Mmark
	parser := parser.NewWithExtensions(extensions)
	output := markdown.ToHTML([]byte(dat), parser, nil)
	return string(output), toc
}
