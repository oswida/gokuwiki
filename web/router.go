package web

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/GeertJohan/go.rice"
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
)

// InitAssetsTemplates initializes the router to use the rice boxes.
// r is our main router, tbox is our template rice box
// and names are the file names of the templates to load
func InitAssetsTemplates(r *gin.Engine, names []string) error {
	var err error
	var tmpl string
	var message *template.Template
	rdr := multitemplate.NewRenderer()

	tbox, _ := rice.FindBox("../templates")
	if tbox != nil {
		for _, x := range names {
			if tmpl, err = tbox.String(x); err != nil {
				return err
			}
			if message, err = template.New(x).Parse(tmpl); err != nil {
				return err
			}
			rdr.Add(x, message)
		}
	} else {
		r.LoadHTMLGlob("templates/*.html")
	}
	aBox, _ := rice.FindBox("../templates/vendor")
	if aBox != nil {
		r.StaticFS("/vendor", aBox.HTTPBox())
	} else {
		r.Static("/vendor", "templates/vendor")
	}
	iBox, _ := rice.FindBox("../templates/css")
	if iBox != nil {
		r.StaticFS("/css", iBox.HTTPBox())
	} else {
		r.Static("/css", "templates/css")
	}
	fBox, _ := rice.FindBox("../templates/webfonts")
	if fBox != nil {
		r.StaticFS("/webfonts", fBox.HTTPBox())
	} else {
		r.Static("/webfonts", "templates/webfonts")
	}
	r.HTMLRender = rdr
	return nil
}

// TPLS - system templates
var TPLS = []string{"page.html", "pagedel.html", "pagecreo.html",
	"pagelist.html", "pagesearch.html", "msg.html", "searchres.html",
	"settings.html", "pageedit.html", "medialist.html", "pagemove.html",
	"mediaselect.html", "pageselect.html"}

// PrepareRoutes - prepare all HTTP routes
func PrepareRoutes(engine *gin.Engine) {
	if err := InitAssetsTemplates(engine, TPLS); err != nil {
		log.Fatal(err)
	}
	for _, pg := range TPLS {
		path := fmt.Sprintf("/%v", pg)
		tpl := pg
		engine.GET(path, func(c *gin.Context) {
			c.HTML(http.StatusOK, tpl, gin.H{})
		})
	}
	engine.GET("/", IndexHandler)
	engine.GET("/page/:path", PageHandler)
	engine.GET("/pagedel/:path", PageDeleteConfirmHandler)
	engine.GET("/del/:path", PageDeleteHandler)
	engine.GET("/pagecreo", PageCreateStartHandler)
	engine.POST("/create", PageCreateHandler)
	engine.POST("/createns", NsCreateHandler)
	engine.GET("/pagelist", PagelistHandler)
	engine.GET("/pagesearch", SearchInputHandler)
	engine.GET("/reindex/:path", ReindexHandler)
	engine.POST("/search", SearchHandler)
	engine.GET("/settings", SettingsHandler)
	engine.POST("/settings-save", SaveSettingsHandler)
	engine.GET("/pageedit/:path", PageEditHandler)
	engine.POST("/edit-save", PageSaveHandler)
	engine.GET("/medialist/:path", MediaListHandler)
	engine.POST("/media-add-folder", MediaAddFolderHandler)
	engine.POST("/media-delete", MediaDeleteHandler)
	engine.POST("/media-upload", MediaUploadHandler)
	engine.GET("/media/:path", MediaHandler)
	engine.GET("/pagemove/:path", PageMoveStartHandler)
	engine.POST("/move", PageMoveHandler)
	engine.GET("/mediaselect/:path", MediaListSelectHandler)
	engine.GET("/pageselect/:path", PageListSelectHandler)
	engine.GET("/image/:path", ImageHandler)
	engine.POST("/savetpl", SaveTemplateHandler)
}
