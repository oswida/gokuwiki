package web

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"../core"
	"github.com/gin-gonic/gin"
)

// IndexHandler - load index page
func IndexHandler(c *gin.Context) {
	template, vars := RenderPage(fmt.Sprintf("%v", core.AppConfig.Globals.NsIndex))
	c.HTML(http.StatusOK, template, vars)
}

// PageHandler - show page
func PageHandler(c *gin.Context) {
	path := c.Param("path")
	name := core.GetPagePath(path)
	if core.IsDir(name) {
		path = fmt.Sprintf("%v:%v", path, core.AppConfig.Globals.NsIndex)
	}
	template, vars := RenderPage(path)
	c.HTML(http.StatusOK, template, vars)
}

// PageDeleteConfirmHandler - confirm page deletion
func PageDeleteConfirmHandler(c *gin.Context) {
	template, vars := RenderPageDelete(c.Param("path"))
	c.HTML(http.StatusOK, template, vars)
}

// PageDeleteHandler - delete page
func PageDeleteHandler(c *gin.Context) {
	err := core.DeletePage(c.Param("path"))
	if err != nil {
		log.Printf("%v\n", err)
	}
	c.Redirect(http.StatusFound, "/")
}

// PageCreateStartHandler - start creating page
func PageCreateStartHandler(c *gin.Context) {
	tpls, _ := core.PageList(core.AppConfig.Globals.TemplateNs, false)
	c.HTML(http.StatusOK, "pagecreo.html", gin.H{"base_font_size": core.AppConfig.Globals.BaseFontSize, "tpllist": tpls})
}

// PageCreateHandler - create a page
func PageCreateHandler(c *gin.Context) {
	pagepath := c.PostForm("pagepath")
	if pagepath == "" {
		c.Redirect(http.StatusFound, "/")
	} else {
		err := core.CreatePage(pagepath, c.PostForm("tpl"))
		if err != nil {
			log.Printf("%v\n", err)
		}
		redir := fmt.Sprintf("/pageedit/%v", pagepath)
		c.Redirect(http.StatusFound, redir)
	}
}

// NsCreateHandler - create namespace
func NsCreateHandler(c *gin.Context) {
	ns := c.PostForm("ns")
	page := c.PostForm("pagename")
	if page == "" {
		c.Redirect(http.StatusFound, "/")
	} else {
		path := page
		if ns != "" {
			path = ns + ":" + path
		}
		err := core.CreatePage(path, c.PostForm("tpl"))
		if err != nil {
			log.Printf("%v\n", err)
		}
		redir := fmt.Sprintf("/pageedit/%v", path)
		c.Redirect(http.StatusFound, redir)
	}
}

// SearchInputHandler - input search term
func SearchInputHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "pagesearch.html", gin.H{"base_font_size": core.AppConfig.Globals.BaseFontSize})
}

// ReindexHandler - reindex page
func ReindexHandler(c *gin.Context) {
	core.IndexPage(c.Param("path"))
	redirect := fmt.Sprintf("/page/%v", c.Param("path"))
	c.Redirect(http.StatusFound, redirect)
}

// PagelistHandler - list pages
func PagelistHandler(c *gin.Context) {
	template, vars := RenderPageString("<pagelist dirheader=\"b\"/>")
	c.HTML(http.StatusOK, template, vars)
}

// SearchHandler - perform search
func SearchHandler(c *gin.Context) {
	term := c.PostForm("term")
	pages := core.WikiSearch(term)
	c.HTML(http.StatusOK, "searchres.html", gin.H{"pages": pages, "base_font_size": core.AppConfig.Globals.BaseFontSize})
}

// SettingsHandler - settings
func SettingsHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "settings.html", gin.H{
		"settings":       core.SettingList,
		"base_font_size": core.AppConfig.Globals.BaseFontSize,
	})
}

// SaveSettingsHandler - save settings
func SaveSettingsHandler(c *gin.Context) {
	core.ApplySettings(c)
	core.SaveConfig()
	core.InitSettings()
	c.Redirect(http.StatusFound, "/")
}

// PageEditHandler - edit page
func PageEditHandler(c *gin.Context) {
	path := c.Param("path")
	text := core.GetRawPageData(path)
	c.HTML(http.StatusOK, "pageedit.html", gin.H{
		"appconfig":      core.AppConfig,
		"base_font_size": core.AppConfig.Globals.BaseFontSize,
		"page_path":      path,
		"content":        text,
		"snippets":       core.EditSnippets,
	})
}

//PageSaveHandler - save edited page
func PageSaveHandler(c *gin.Context) {
	content := c.PostForm("editor")
	pagepath := c.PostForm("pagepath")
	err := core.SavePage(pagepath, content)
	if err != nil {
		log.Printf("%v\n", err)
	}
	c.Redirect(http.StatusFound, "/page/"+pagepath)
}

// MediaListHandler - list media
func MediaListHandler(c *gin.Context) {
	path := c.Param("path")
	if path == "_" {
		path = ""
	}
	list := core.ListMedia(path)
	hasback := true
	if path == "" {
		path = core.MediaRootTitle
		hasback = false
	}
	backlink := core.Ns(c.Param("path"))
	if backlink == "" {
		backlink = "_"
	}
	icon, _ := core.FiletypeIcon(core.GetPagePath(strings.Replace(c.Param("path"), "_", "", 1)))
	c.HTML(http.StatusOK, "medialist.html", gin.H{
		"page_path": path,
		"medialist": list,
		"hasback":   hasback,
		"backlink":  backlink,
		"icon":      icon,
	})
}

// MediaAddFolderHandler - add media folder
func MediaAddFolderHandler(c *gin.Context) {
	pagepath := c.PostForm("pagepath")
	foldername := c.PostForm("foldername")
	var creo string
	if pagepath != core.MediaRootTitle {
		creo = pagepath + ":" + foldername
	} else {
		creo = foldername
	}
	core.CreateMediaFolder(creo)
	if pagepath == core.MediaRootTitle {
		pagepath = "_"
	}
	c.Redirect(http.StatusFound, "/medialist/"+pagepath)
}

// MediaDeleteHandler - delete media
func MediaDeleteHandler(c *gin.Context) {
	pagepath := c.PostForm("pagepath")
	filename := c.PostForm("filename")
	var del string
	if pagepath != core.MediaRootTitle {
		del = pagepath + ":" + filename
	} else {
		del = filename
	}
	core.DeleteMediaFolder(del)
	if pagepath == core.MediaRootTitle {
		pagepath = "_"
	}
	c.Redirect(http.StatusFound, "/medialist/"+pagepath)
}

// MediaUploadHandler - upload new media
func MediaUploadHandler(c *gin.Context) {
	file, err := c.FormFile("filename")
	pagepath := c.PostForm("pagepath")
	// log.Printf("upload %v %v", pagepath, file.Filename)
	if pagepath == core.MediaRootTitle {
		pagepath = ""
	}
	if err == nil {
		pth := core.GetMediaPath() + string(os.PathSeparator) +
			strings.ReplaceAll(pagepath, ":", string(os.PathSeparator))
		err = c.SaveUploadedFile(file, pth+string(os.PathSeparator)+file.Filename)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Fatal(err)
	}
	pagepath = c.PostForm("pagepath")
	if pagepath == core.MediaRootTitle {
		pagepath = "_"
	}
	c.Redirect(http.StatusFound, "/medialist/"+pagepath)
}

// MediaHandler - show media file
func MediaHandler(c *gin.Context) {
	path := c.Param("path")
	filepath := core.GetMediaPath() + string(os.PathSeparator) +
		strings.ReplaceAll(path, ":", string(os.PathSeparator))
	//file, _ := os.Open(filepath)
	// c.FileAttachment(filepath, core.LastAtFsPath(file.Name()))
	// file.Close()
	c.File(filepath)
}

// PageMoveStartHandler - start moving page
func PageMoveStartHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "pagemove.html", gin.H{
		"base_font_size": core.AppConfig.Globals.BaseFontSize,
		"pagepath":       c.Param("path"),
	})
}

// PageMoveHandler - move a page
func PageMoveHandler(c *gin.Context) {
	err := core.MovePage(c.PostForm("frompath"), c.PostForm("pagepath"), true)
	if err != nil {
		log.Printf("%v\n", err)
	}
	redir := fmt.Sprintf("/page/%v", c.PostForm("pagepath"))
	c.Redirect(http.StatusFound, redir)
}

// MediaListSelectHandler - page for selecting media in modal dialog
func MediaListSelectHandler(c *gin.Context) {
	path := c.Param("path")
	if path == "_" {
		path = ""
	}
	list := core.ListMedia(path)
	hasback := true
	if path == "" {
		path = core.MediaRootTitle
		hasback = false
	}
	backlink := core.Ns(c.Param("path"))
	if backlink == "" {
		backlink = "_"
	}
	icon, _ := core.FiletypeIcon(core.GetPageMediaPath(strings.Replace(c.Param("path"), "_", "", 1)))
	c.HTML(http.StatusOK, "mediaselect.html", gin.H{
		"page_path": path,
		"medialist": list,
		"hasback":   hasback,
		"backlink":  backlink,
		"icon":      icon,
	})
}

// PageListSelectHandler - page for selecting pages in modal dialog
func PageListSelectHandler(c *gin.Context) {
	path := c.Param("path")
	if path == "_" {
		path = ""
	}
	list, _ := core.PageList(path, false)
	hasback := true
	if path == "" {
		hasback = false
	}
	backlink := core.Ns(c.Param("path"))
	if backlink == "" {
		backlink = "_"
	}
	c.HTML(http.StatusOK, "pageselect.html", gin.H{
		"page_path": path,
		"pagelist":  list,
		"hasback":   hasback,
		"backlink":  backlink,
	})
}

// ImageHandler - send image
func ImageHandler(c *gin.Context) {
	path := c.Param("path")
	filepath := core.GetMediaPath() + string(os.PathSeparator) +
		strings.ReplaceAll(path, ":", string(os.PathSeparator))
	//file, _ := os.Open(filepath)
	// c.FileAttachment(filepath, core.LastAtFsPath(file.Name()))
	// file.Close()
	c.File(filepath)
}

// SaveTemplateHandler - save page as template
func SaveTemplateHandler(c *gin.Context) {
	if c.PostForm("tplname") != "" {
		dest := core.AppConfig.Globals.TemplateNs + ":" + c.PostForm("tplname")
		err := core.MovePage(c.PostForm("pagename"), dest, false)
		if err != nil {
			log.Printf("%v\n", err)
		}
		redir := fmt.Sprintf("/page/%v", dest)
		c.Redirect(http.StatusFound, redir)
	} else {
		c.Redirect(http.StatusFound, "")
	}
}
