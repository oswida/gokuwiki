package plugins

import (
	"fmt"
	"strconv"

	"../core"
	"github.com/gin-gonic/gin"
)

// RenderGallery - render gallery of images
func RenderGallery(plugin *GokuPlugin, content string, ns string) string {
	res := plugin.FindElement("gallery", content)
	replace := []ReplaceElement{}
	for _, el := range res {
		dir, ok := el.Attrs["ns"]
		if !ok {
			dir = ns
		}
		list := core.ListMedia(dir)
		maxrow := 0
		val, ok := el.Attrs["maxrow"]
		if ok {
			maxrow, _ = strconv.Atoi(val)
		}
		margin := ""
		val, ok = el.Attrs["margin"]
		if ok {
			margin = "style=\"margin: " + val + "px\""
		}
		if len(list) > 0 {
			e := ReplaceElement{}
			e.Replace = ""
			cnt := 0
			for _, item := range list {
				if item.Icon == "fas fa-file-image" {
					br := ""
					if cnt != 0 && cnt == maxrow-1 {
						br = "<br/>"
					}
					e.Position = el.Position
					e.Replace = e.Replace +
						fmt.Sprintf("<a href=\"/media/%v\"><img src=\"/image/%v\" %v %v %v></a>%v",
							item.Path, item.Path, widthAttr(el.Attrs), heightAttr(el.Attrs), margin, br)
					cnt++
					if cnt == maxrow {
						cnt = 0
					}
				}
			}
			e.Replace = e.Replace + ""
			e.Replace = "<div " + styleColumn(el.Attrs) + ">" + e.Replace + "</div>"
			if len(e.Position) > 0 {
				replace = append(replace, e)
			}
		}
	}
	return ReplaceData(content, replace)
}

// RoutesGallery -
func RoutesGallery(plugin *GokuPlugin, engine *gin.Engine) {

}

// PluginGalleryCreate - Create Highlight plugin
func PluginGalleryCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "Gallery",
		RenderFunc: RenderGallery,
		RoutesFunc: RoutesGallery,
	}
}
