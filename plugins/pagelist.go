package plugins

import (
	"fmt"
	"strings"

	"../core"
	"github.com/gin-gonic/gin"
)

func pageLink(page core.PageInfo, attrs map[string]string) string {
	nm := page.Name
	link := page.Path
	if page.IsDir {
		link = fmt.Sprintf("%v:%v", page.Path, core.AppConfig.Globals.NsIndex)
		nm = dirheaderAttr(attrs, nm)
	}
	return "<a href=\"/page/" + link + "\">" + nm + "</a>"
}

func appendChildren(page core.PageInfo, children []core.PageInfo,
	content string, attrs map[string]string, depth int) string {
	if depth <= 0 {
		return content
	}
	result := content + "<ul " + styleAttr(attrs) + ">"
	for _, child := range children {
		parts := strings.Split(child.Path, ":")
		if parts[len(parts)-1] != core.AppConfig.Globals.NsIndex {
			result = result + "<li>" + pageLink(child, attrs)
			if child.IsDir {
				result = appendChildren(child, child.Children, result, attrs, depth-1)
			}
			result = result + "</li>"
		}
	}
	return result + "</ul>"
}

// RenderPagelist - Render page list of a specified default namespace
func RenderPagelist(plugin *GokuPlugin, content string, ns string) string {
	res := plugin.FindElement("pagelist", content)
	replace := []ReplaceElement{}
	for _, el := range res {
		e := ReplaceElement{}
		e.Position = el.Position
		e.Replace = "<ul " + styleAttr(el.Attrs) + ">"
		depth := depthAttr(el.Attrs)
		namespace := nsAttr(el.Attrs)
		if namespace == "" {
			namespace = ns
		}
		list, _ := core.PageList(namespace, true)
		for _, page := range list {
			parts := strings.Split(page.Path, ":")
			if parts[len(parts)-1] != core.AppConfig.Globals.NsIndex {
				e.Replace = e.Replace + "<li>" + pageLink(page, el.Attrs)
				if page.IsDir && depth > 0 {
					e.Replace = appendChildren(page, page.Children, e.Replace, el.Attrs, depth)
				}
				e.Replace = e.Replace + "</li>"
			}
		}
		e.Replace = e.Replace + "</ul>"
		e.Replace = "<div " + styleColumn(el.Attrs) + ">" + e.Replace + "</div>"
		replace = append(replace, e)
	}

	return ReplaceData(content, replace)
}

// RoutesPagelist - routes
func RoutesPagelist(plugin *GokuPlugin, engine *gin.Engine) {
}

// PluginPagelistCreate - create page list plugin
func PluginPagelistCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "Pagelist",
		RenderFunc: RenderPagelist,
		RoutesFunc: RoutesPagelist,
	}
}
