package plugins

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// var HighlightStyle = "background: #dddddd; color: #000000;padding: 2px;"
var HighlightStyle = "class=\"badge badge-light\""
var HighlightStyleDark = "class=\"badge badge-dark\""
var HighlightStyleInfo = "class=\"badge badge-info\""

// RenderHighlight - render highlight
func RenderHighlight(plugin *GokuPlugin, content string, ns string) string {
	cnt := content
	res := plugin.FindElementWithContent("inf", cnt)
	replace := []ReplaceElement{}
	for _, el := range res {
		e := ReplaceElement{}
		e.Position = el.Position
		e.Replace = fmt.Sprintf("<span %v >%v</span>", HighlightStyleInfo, el.Content)
		replace = append(replace, e)
	}
	cnt = ReplaceData(cnt, replace)
	replace = []ReplaceElement{}
	res = plugin.FindElementWithContent("dhl", cnt)
	for _, el := range res {
		e := ReplaceElement{}
		e.Position = el.Position
		e.Replace = fmt.Sprintf("<span %v >%v</span>", HighlightStyleDark, el.Content)
		replace = append(replace, e)
	}
	cnt = ReplaceData(cnt, replace)
	replace = []ReplaceElement{}
	res = plugin.FindElementWithContent("hl", cnt)
	for _, el := range res {
		e := ReplaceElement{}
		e.Position = el.Position
		e.Replace = fmt.Sprintf("<span %v >%v</span>", HighlightStyle, el.Content)
		replace = append(replace, e)
	}
	return ReplaceData(cnt, replace)
}

// RoutesHighlight -
func RoutesHighlight(plugin *GokuPlugin, engine *gin.Engine) {

}

// PluginHighlightCreate - Create Highlight plugin
func PluginHighlightCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "Highlight",
		RenderFunc: RenderHighlight,
		RoutesFunc: RoutesHighlight,
	}
}
