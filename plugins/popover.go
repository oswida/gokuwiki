package plugins

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

var PopoverTemplate = "<a tabindex=\"0\"  data-toggle=\"popover\" %v  %v  title=\"%v\" data-html=\"true\" data-content=\"%v\">%v</a>"

// RenderPopover - render thumbnail image
func RenderPopover(plugin *GokuPlugin, content string, ns string) string {
	res := plugin.FindElementWithContent("popover", content)
	replace := []ReplaceElement{}
	for _, el := range res {
		e := ReplaceElement{}
		e.Position = el.Position
		e.Replace = fmt.Sprintf(PopoverTemplate, btnClassAttr(el.Attrs), focusAttr(el.Attrs),
			titleAttr(el.Attrs), el.Content, titleAttr(el.Attrs))
		replace = append(replace, e)
	}
	return ReplaceData(content, replace)
}

// RoutesPopover -
func RoutesPopover(plugin *GokuPlugin, engine *gin.Engine) {

}

// PluginPopoverCreate - Create AddPage plugin
func PluginPopoverCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "Popover",
		RenderFunc: RenderPopover,
		RoutesFunc: RoutesPopover,
	}
}
