package plugins

import (
	"fmt"

	"../core"
	"github.com/gin-gonic/gin"
)

// RenderInclude - render include plugin
func RenderInclude(plugin *GokuPlugin, content string, ns string) string {
	res := plugin.FindElement("include", content)
	replace := []ReplaceElement{}
	for _, el := range res {
		path, ok := el.Attrs["path"]
		if ok {
			e := ReplaceElement{}
			e.Position = el.Position
			content := core.GetRawPageData(path)
			e.Replace = fmt.Sprintf("%v", content)
			replace = append(replace, e)
		}
	}
	return ReplaceData(content, replace)
}

// RoutesInclude -
func RoutesInclude(plugin *GokuPlugin, engine *gin.Engine) {

}

// PluginIncludeCreate - Create Include plugin
func PluginIncludeCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "Include",
		RenderFunc: RenderInclude,
		RoutesFunc: RoutesInclude,
	}
}
