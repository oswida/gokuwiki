package plugins

import (
	"../core"
	"github.com/gin-gonic/gin"
)

// RenderMedialist - Render media list of a specified namespace
// Note: this is a flat list containing only files
func RenderMedialist(plugin *GokuPlugin, content string, ns string) string {
	res := plugin.FindElement("medialist", content)
	replace := []ReplaceElement{}
	for _, el := range res {
		dir, ok := el.Attrs["ns"]
		if !ok {
			dir = ns
		}
		list := core.ListMedia(dir)
		if len(list) > 0 {
			e := ReplaceElement{}
			e.Replace = "<ul style=\"list-style-type:none\">"
			for _, item := range list {
				if !item.IsDir {
					e.Position = el.Position
					e.Replace = e.Replace + "<li><i class=\"" + item.Icon + "\"></i> <a href=\"/media/" + item.Path +
						"\"> " + item.Name + "</a></li>"
				}
			}
			e.Replace = e.Replace + "</ul>"
			e.Replace = "<div " + styleColumn(el.Attrs) + ">" + e.Replace + "</div>"
			replace = append(replace, e)
		}
	}
	return ReplaceData(content, replace)
}

// RoutesMedialist - routes
func RoutesMedialist(plugin *GokuPlugin, engine *gin.Engine) {
}

// PluginMedialistCreate - create media list plugin
func PluginMedialistCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "Medialist",
		RenderFunc: RenderMedialist,
		RoutesFunc: RoutesMedialist,
	}
}
