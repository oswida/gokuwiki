package plugins

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

var FoldTemplate = "<i class=\"fas fa-chevron-right\" style=\"margin-right: 0.2em;font-size: 0.7em;vertical-align:center;\"></i> <a data-toggle=\"collapse\" href=\"#%v\" %v >%v </a><div class=\"collapse\" id=\"%v\" ><div style=\"margin-bottom: 0.5em;border: 1px dotted #525a5c; padding: 5px;\">%v</div></div>"

// RenderFold - render folded area
func RenderFold(plugin *GokuPlugin, content string, ns string) string {
	res := plugin.FindElementWithContent("fold", content)
	replace := []ReplaceElement{}
	for idx, el := range res {
		e := ReplaceElement{}
		id := fmt.Sprintf("fold%v", idx)
		e.Position = el.Position
		e.Replace = fmt.Sprintf(FoldTemplate,
			id, btnClassAttr(el.Attrs), titleAttr(el.Attrs),
			id, el.Content)
		replace = append(replace, e)
	}
	return ReplaceData(content, replace)
}

// RoutesFold -
func RoutesFold(plugin *GokuPlugin, engine *gin.Engine) {

}

// PluginFoldCreate - Create Fold plugin
func PluginFoldCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "Fold",
		RenderFunc: RenderFold,
		RoutesFunc: RoutesFold,
	}
}
