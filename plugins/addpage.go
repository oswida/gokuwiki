package plugins

import (
	"fmt"

	"../core"
	"github.com/gin-gonic/gin"
)

const TplPart = `
<span class="input-group-btn" style="margin-left: 5px">
<select name="tpl" class="form-control btn-secondary">
<option value="">--- Blank page ---</option>
%v
</select>
</span>
`
const AddPageForm = `<form method="POST" action="/createns" style="width: 30%%">
<input type="hidden" name="ns" value="%v"/>
<div class="input-group">
<input type="text" class="form-control" name="pagename">
%v
<span class="input-group-btn">
<button class="btn btn-primary" type="submit" style="margin-left: 5px">Create</button>
</span>
</div>
</form>`

// RenderAddPage - render add page plugin
func RenderAddPage(plugin *GokuPlugin, content string, ns string) string {
	res := plugin.FindElement("addpage", content)
	replace := []ReplaceElement{}
	for _, el := range res {
		tpl := ""
		hastpl, ok := el.Attrs["hastpl"]
		if ok && hastpl == "true" {
			tpls, _ := core.PageList(core.AppConfig.Globals.TemplateNs, false)
			txt := ""
			for _, t := range tpls {
				txt = txt + fmt.Sprintf("<option value=\"%v\">%v</option>", t.Path, t.Path)
			}
			tpl = fmt.Sprintf(TplPart, txt)
		}
		e := ReplaceElement{}
		e.Position = el.Position
		e.Replace = fmt.Sprintf(AddPageForm, ns, tpl)
		replace = append(replace, e)
	}
	return ReplaceData(content, replace)
}

// RoutesAddPage -
func RoutesAddPage(plugin *GokuPlugin, engine *gin.Engine) {

}

// PluginAddPageCreate - Create AddPage plugin
func PluginAddPageCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "AddPage",
		RenderFunc: RenderAddPage,
		RoutesFunc: RoutesAddPage,
	}
}
