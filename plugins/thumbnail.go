package plugins

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// RenderThumbnail - render thumbnail image
func RenderThumbnail(plugin *GokuPlugin, content string, ns string) string {
	res := plugin.FindElement("thumbnail", content)
	replace := []ReplaceElement{}
	for _, el := range res {
		e := ReplaceElement{}
		e.Position = el.Position
		e.Replace = fmt.Sprintf("<img src=\"/image/%v\" %v %v />", el.Attrs["src"], widthAttr(el.Attrs), heightAttr(el.Attrs))
		e.Replace = "<a href=\"/media/" + el.Attrs["src"] + "\">" + e.Replace + "</a>"
		replace = append(replace, e)
	}
	return ReplaceData(content, replace)
}

// RoutesThumbnail -
func RoutesThumbnail(plugin *GokuPlugin, engine *gin.Engine) {

}

// PluginThumbnailCreate - Create AddPage plugin
func PluginThumbnailCreate() GokuPlugin {
	return GokuPlugin{
		Name:       "Thumbnail",
		RenderFunc: RenderThumbnail,
		RoutesFunc: RoutesThumbnail,
	}
}
