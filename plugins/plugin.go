package plugins

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	// "log"
	"regexp"
)

// GokuPlugin - plugin structure
type GokuPlugin struct {
	Name       string
	RenderFunc func(plugin *GokuPlugin, content string, ns string) string
	RoutesFunc func(plugin *GokuPlugin, engine *gin.Engine)
}

// PluginElement - plugin element in text
type PluginElement struct {
	Position []int
	Attrs    map[string]string
	Content  string
}

// FindElement - find specific XML element
func (plugin GokuPlugin) FindElement(name string, content string) []PluginElement {
	result := []PluginElement{}
	rt := fmt.Sprintf("<%s[^>]*>", name)
	reg, _ := regexp.Compile(rt)
	matches := reg.FindAllStringIndex(content, -1)
	for _, elem := range matches {
		txt := content[elem[0]:elem[1]]
		pe := PluginElement{}
		pe.Position = []int{elem[0], elem[1]}
		pe.Attrs = map[string]string{}
		attrreg := regexp.MustCompile(`([a-zA-Z0-9-]+)="([^"]+)"`)
		attrs := attrreg.FindAllStringSubmatchIndex(txt, -1)
		for _, attr := range attrs {
			pe.Attrs[txt[attr[2]:attr[3]]] = txt[attr[4]:attr[5]]
		}
		result = append(result, pe)
	}
	return result
}

// FindText - find specific text
func (plugin GokuPlugin) FindText(name string, content string) []PluginElement {
	result := []PluginElement{}
	rt := fmt.Sprintf("\\s+(%v)\\s+", name)
	reg, _ := regexp.Compile(rt)
	matches := reg.FindAllStringSubmatchIndex(content, -1)
	for _, elem := range matches {
		pe := PluginElement{}
		pe.Content = content[elem[2]:elem[3]]
		pe.Position = []int{elem[2], elem[3]}
		result = append(result, pe)
	}
	return result
}

// FindMarks - find specific marks in text
func (plugin GokuPlugin) FindMarks(prefix string, suffix string, content string) []PluginElement {
	result := []PluginElement{}
	chars := prefix
	if prefix != suffix {
		chars = chars + suffix
	}
	rt := fmt.Sprintf("%s([^%s]+)%s", prefix, chars, suffix)
	reg := regexp.MustCompile(rt)
	matches := reg.FindAllStringSubmatchIndex(content, -1)
	for _, elem := range matches {
		pe := PluginElement{}
		pe.Content = content[elem[2]:elem[3]]
		pe.Position = []int{elem[0], elem[1]}
		if pe.Content != "" {
			result = append(result, pe)
		}
	}
	return result
}

// FindElementWithContent - find specific XML element with content
func (plugin GokuPlugin) FindElementWithContent(name string, content string) []PluginElement {
	result := []PluginElement{}
	rt := fmt.Sprintf("(?s)<%s([^>]*)>([^<>]*)</%s>", name, name)
	reg, _ := regexp.Compile(rt)
	matches := reg.FindAllStringSubmatchIndex(content, -1)
	for _, elem := range matches {
		txt := content[elem[2]:elem[3]]
		pe := PluginElement{}
		pe.Position = []int{elem[0], elem[1]}
		pe.Attrs = map[string]string{}
		attrreg := regexp.MustCompile(`([a-zA-Z0-9-]+)="([^"]+)"`)
		attrs := attrreg.FindAllStringSubmatchIndex(txt, -1)
		for _, attr := range attrs {
			pe.Attrs[txt[attr[2]:attr[3]]] = txt[attr[4]:attr[5]]
		}
		pe.Content = content[elem[4]:elem[5]]
		result = append(result, pe)
	}
	return result
}

// ReplaceElement - elelent for replace
type ReplaceElement struct {
	Position []int
	Replace  string
}

// ReplaceData - replace data
func ReplaceData(text string, elements []ReplaceElement) string {
	content := text
	adjustment := 0
	for _, el := range elements {
		origin := content[el.Position[0]+adjustment : el.Position[1]+adjustment]
		content = content[:el.Position[0]+adjustment] + el.Replace + content[el.Position[1]+adjustment:]
		adjustment += len(el.Replace) - len(origin)
	}
	return content
}

func dirheaderAttr(attrs map[string]string, name string) string {
	val, ok := attrs["dirheader"]
	if ok {
		return "<" + val + ">" + name + "</" + val + ">"
	}
	return name
}

func depthAttr(attrs map[string]string) int {
	val, ok := attrs["depth"]
	depth := 0
	var err error
	if ok {
		depth, err = strconv.Atoi(val)
		if err != nil {
			depth = 1000
		}
	} else {
		depth = 1000
	}
	return depth
}

func styleAttr(attrs map[string]string) string {
	val, ok := attrs["style"]
	if ok {
		return "style=\"list-style-type:" + val + ";\""
	} else {
		return ""
	}
}

func styleColumn(attrs map[string]string) string {
	col, ok := attrs["columns"]
	if ok {
		return "style=\"column-count:" + col + "\""
	}
	return ""
}

func widthAttr(attrs map[string]string) string {
	val, ok := attrs["width"]
	if ok {
		return "width=\"" + val + "\""
	}
	return ""
}

func heightAttr(attrs map[string]string) string {
	val, ok := attrs["height"]
	if ok {
		return "height=\"" + val + "\""
	}
	return ""
}

func titleAttr(attrs map[string]string) string {
	val, ok := attrs["title"]
	if ok {
		return val
	}
	return ""
}

func nsAttr(attrs map[string]string) string {
	val, ok := attrs["ns"]
	if ok {
		return val
	}
	return ""
}

func focusAttr(attrs map[string]string) string {
	val, ok := attrs["focus"]
	if ok && val == "true" {
		return "data-trigger=\"focus\""
	}
	return ""
}

func btnClassAttr(attrs map[string]string) string {
	val, ok := attrs["btn-class"]
	if ok {
		switch val {
		case "primary":
			return "class=\"btn btn-primary\""
		case "secondary":
			return "class=\"btn btn-secondary\""
		}
	}
	return ""
}
