package main

import (
	"fmt"

	"./core"
	"./web"
	"github.com/blevesearch/bleve"
	"github.com/gin-gonic/gin"
	"github.com/pkg/browser"
)

// StartServer - start gokuwiki server
func StartServer() {
	// Run server
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	web.PrepareRoutes(r)
	web.InitPlugins(r)
	if core.AppConfig.Globals.LaunchBrowser {
		launchBrowser()
	}
	err := r.Run(fmt.Sprintf("localhost:%v", core.AppConfig.Globals.UiPort))
	if err != nil {
		panic(err)
	}
}

func launchBrowser() {
	err := browser.OpenURL(fmt.Sprintf("http://localhost:%v", core.AppConfig.Globals.UiPort))
	if err != nil {
		panic(err)
	}
}

func main() {
	if core.AndroidMode {
		// AndroidRun()
	} else {
		core.LoadConfig()
		core.InitSettings()
		core.CreateDirs()
		name := fmt.Sprintf("%v/%v", core.CurrentDir, "index.bleve")
		mapping := bleve.NewIndexMapping()
		_, err := bleve.New(name, mapping)
		if err != nil {
			fmt.Print(err)
		}
		StartServer()
	}
}
