package core

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

// Setting - single setting param
type Setting struct {
	Name        string
	Placeholder string
	ConfigName  string
	Value       string
	Type        string
}

// SettingList - list of settings
var SettingList []Setting

// InitSettings - initialize settings map
func InitSettings() {
	SettingList = []Setting{}
	SettingList = append(SettingList,
		Setting{"BaseFontSize", "Base font size", "BaseFontSize", AppConfig.Globals.BaseFontSize, "number"})
	SettingList = append(SettingList,
		Setting{"TemplateNs", "Template namespace", "TemplateNs", AppConfig.Globals.TemplateNs, "text"})
	SettingList = append(SettingList,
		Setting{"EditorShowLines", "Show lines in editor", "EditorShowLines", strconv.FormatBool(AppConfig.Editor.ShowLineNumbers), "checkbox"})
	SettingList = append(SettingList,
		Setting{"EditorWrapLines", "Wrap lines in editor", "EditorWrapLines", strconv.FormatBool(AppConfig.Editor.WrapLines), "checkbox"})
}

// ApplySettings - apply settings from form to structure
func ApplySettings(c *gin.Context) {
	AppConfig.Globals.BaseFontSize = c.PostForm("BaseFontSize")
	AppConfig.Globals.TemplateNs = c.PostForm("TemplateNs")
	AppConfig.Editor.ShowLineNumbers = c.PostForm("EditorShowLines") == "on"
	AppConfig.Editor.WrapLines = c.PostForm("EditorWrapLines") == "on"
}
