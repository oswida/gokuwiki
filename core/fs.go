package core

import (
	"fmt"
	"sort"

	"io/ioutil"
	"os"
	"strings"

	"github.com/blevesearch/bleve"
)

// FileExists - check if file exists
func FileExists(filename string) bool {
	_, err := os.Stat(filename)
	return !os.IsNotExist(err)
}

// IsDir - check if file exists and is dir
func IsDir(filename string) bool {
	info, err := os.Stat(filename)
	if err != nil {
		return false
	}
	return info.IsDir()
}

// GetDataPath - get current data dir path
func GetDataPath() string {
	return fmt.Sprintf("%v%v%v", CurrentDir, string(os.PathSeparator), AppConfig.Dirs.Data)
}

// GetMediaPath - get current data dir path
func GetMediaPath() string {
	return fmt.Sprintf("%v%v%v", CurrentDir, string(os.PathSeparator), AppConfig.Dirs.Media)
}

// GetPagePath - get fs path for page
func GetPagePath(pagename string) string {
	return fmt.Sprintf("%v%v%v", GetDataPath(), string(os.PathSeparator),
		strings.ReplaceAll(pagename, ":", string(os.PathSeparator)))
}

// GetPageMediaPath - get fs media path for page
func GetPageMediaPath(pagename string) string {
	return fmt.Sprintf("%v%v%v", GetMediaPath(), string(os.PathSeparator),
		strings.ReplaceAll(pagename, ":", string(os.PathSeparator)))
}

// GetPagePathNs - get page fs path based on namespace
func GetPagePathNs(namespace string, page string) string {
	pagename := namespace + ":" + page
	return GetPagePath(pagename)
}

// GetRawPageData - return raw text from page file.
func GetRawPageData(pagepath string) string {
	var err error
	name := GetPagePath(pagepath)
	if IsDir(name) {
		name = GetPagePathNs(pagepath, fmt.Sprintf("%v", AppConfig.Globals.NsIndex))
	}
	if !FileExists(name) {
		return ""
	}
	dat, err := ioutil.ReadFile(name)
	if err != nil {
		return ""
	}
	return string(dat)
}

// CreateNamespace - create new folder and start page
func CreateNamespace(nsname string, withStart bool) error {
	var err error
	name := GetPagePath(nsname)
	if !FileExists(name) {
		err = os.MkdirAll(name, os.ModePerm)
	}
	if withStart {
		startpage := GetPagePathNs(nsname, fmt.Sprintf("%v", AppConfig.Globals.NsIndex))
		if !FileExists(startpage) {
			_, err = os.Create(startpage)
		}
	}
	return err
}

// ListNamespacePages - list all namespace files
func ListNamespacePages(nsname string) []string {
	name := GetPagePath(nsname)
	if !FileExists(name) {
		return []string{}
	}
	dir, err := os.Open(name)
	if err != nil {
		return []string{}
	}
	list, _ := dir.Readdir(0)
	var result []string
	for _, item := range list {
		if !item.IsDir() {
			result = append(result, item.Name())
		}
	}
	return result
}

// ListNamespaceSubs - list all namespace folders
func ListNamespaceSubs(nsname string) []string {
	name := GetPagePath(nsname)
	if !FileExists(name) {
		return []string{}
	}
	dir, err := os.Open(name)
	if err != nil {
		return []string{}
	}
	list, _ := dir.Readdir(0)
	var result []string
	for _, item := range list {
		if item.IsDir() {
			result = append(result, item.Name())
		}
	}
	return result
}

// HasSidebar - check if sidebar page is defined
func HasSidebar() bool {
	return false
}

// DeletePage - delete page from disk
func DeletePage(pagename string) error {
	name := GetPagePath(pagename)
	if !FileExists(name) {
		return fmt.Errorf("File does not exist %v", name)
	}
	os.Remove(name)
	return nil
}

// CreatePage - create page on disk
func CreatePage(pagename string, template string) error {
	name := GetPagePath(pagename)
	if FileExists(name) {
		return fmt.Errorf("File already exists %v", name)
	}
	// parts := strings.Split(pagename, "/")
	// parts = parts[:len(parts)-1]
	// dir := strings.Join(parts, "/")
	dir := Ns(pagename)
	ns := GetPagePath(dir)
	if !FileExists(ns) {
		os.MkdirAll(ns, os.ModePerm)
		CreateMediaFolder(dir)
	}
	media := GetPageMediaPath(dir)
	if !FileExists(media) {
		os.MkdirAll(media, os.ModePerm)
	}
	file, _ := os.Create(name)
	if template != "" {
		data := GetRawPageData(template)
		file.WriteString(data)
	}
	file.Close()
	return nil
}

// PageInfo - info about page
type PageInfo struct {
	Name     string
	Path     string
	FsPath   string
	Children []PageInfo
	IsDir    bool
}

// DirList - make dir list
func DirList(dir *PageInfo) {
	prefix := GetDataPath()
	files, _ := ioutil.ReadDir(dir.FsPath)
	for _, file := range files {
		pth := strings.Replace(dir.FsPath+string(os.PathSeparator)+file.Name(), prefix, "", 1)
		uri := strings.ReplaceAll(pth, string(os.PathSeparator), ":")
		for strings.HasPrefix(uri, ":") {
			uri = strings.Replace(uri, ":", "", 1)
		}
		if file.IsDir() {
			child := PageInfo{file.Name(), uri, GetDataPath() + string(os.PathSeparator) + pth, []PageInfo{}, true}
			header := FindFirstHeader(GetRawPageData(fmt.Sprintf("%v:%v", child.Path, AppConfig.Globals.NsIndex)))
			if header != "" {
				child.Name = header
			}
			DirList(&child)
			dir.Children = append(dir.Children, child)
		} else {
			page := PageInfo{file.Name(), uri, pth, []PageInfo{}, false}
			header := FindFirstHeader(GetRawPageData(page.Path))
			if header != "" {
				page.Name = header
			}
			dir.Children = append(dir.Children, page)
		}
	}
	sort.Slice(dir.Children, func(i, j int) bool {
		a := dir.Children[i]
		b := dir.Children[j]
		switch {
		case a.IsDir && !b.IsDir:
			return true
		case !a.IsDir && b.IsDir:
			return false
		default:
			return strings.ToUpper(a.Name) < strings.ToUpper(b.Name)
		}
	})
}

// PageList - get page list
func PageList(ns string, recursive bool) ([]PageInfo, error) {
	dir := GetDataPath()
	if ns != "" {
		dir = dir + string(os.PathSeparator) + strings.ReplaceAll(ns, ":", string(os.PathSeparator))
	}
	result := []PageInfo{}
	files, _ := ioutil.ReadDir(dir)
	for _, fl := range files {
		uri := ns + ":" + fl.Name()
		for strings.HasPrefix(uri, ":") {
			uri = strings.Replace(uri, ":", "", 1)
		}
		fspath := dir + string(os.PathSeparator) + fl.Name()
		if fl.IsDir() {
			child := PageInfo{fl.Name(), uri, fspath, []PageInfo{}, true}
			header := FindFirstHeader(GetRawPageData(fmt.Sprintf("%v:%v", child.Path, AppConfig.Globals.NsIndex)))
			if header != "" {
				child.Name = header
			}
			if recursive {
				DirList(&child)
			}
			result = append(result, child)
		} else {
			page := PageInfo{fl.Name(), uri, fspath, []PageInfo{}, false}
			header := FindFirstHeader(GetRawPageData(page.Path))
			if header != "" {
				page.Name = header
			}
			result = append(result, page)
		}
	}
	sort.Slice(result, func(i, j int) bool {
		a := result[i]
		b := result[j]
		switch {
		case a.IsDir && !b.IsDir:
			return true
		case !a.IsDir && b.IsDir:
			return false
		default:
			return strings.ToUpper(a.Name) < strings.ToUpper(b.Name)
		}
	})
	return result, nil
}

// IndexPage - index wiki page
func IndexPage(pagename string) {
	text := GetRawPageData(pagename)
	name := fmt.Sprintf("%v%v%v", CurrentDir, string(os.PathSeparator), "index.bleve")
	index, _ := bleve.Open(name)
	index.Index(pagename, text)
	index.Close()
}

// SavePage - save markdown page to file
func SavePage(pagename string, content string) error {
	name := GetPagePath(pagename)
	if !FileExists(name) {
		CreatePage(pagename, "")
		return fmt.Errorf("File not found for page %v", pagename)
	}
	ioutil.WriteFile(name, []byte(strings.ReplaceAll(content, "\r", "")), os.ModePerm)
	IndexPage(pagename)
	return nil
}

// MovePage - move page on disk
func MovePage(frompath string, topath string, delpage bool) error {
	fsfrom := GetPagePath(frompath)
	fsto := GetPagePath(topath)
	if FileExists(fsto) {
		return fmt.Errorf("File already exists %v", fsto)
	}
	dir := Ns(topath)
	ns := GetPagePath(dir)
	if !FileExists(ns) {
		os.MkdirAll(ns, os.ModePerm)
		CreateMediaFolder(dir)
	}
	media := GetPageMediaPath(dir)
	if !FileExists(media) {
		os.MkdirAll(media, os.ModePerm)
	}
	data, err := ioutil.ReadFile(fsfrom)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(fsto, data, os.ModePerm)
	if err != nil {
		return err
	}
	if delpage {
		err = os.Remove(fsfrom)
		if err != nil {
			return err
		}
	}
	return nil
}
