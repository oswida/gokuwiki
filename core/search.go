package core

import (
	"fmt"
	"github.com/blevesearch/bleve"
	"strings"
)

// WikiSearch - search wiki for term
func WikiSearch(term string) []string {
	name := fmt.Sprintf("%v/%v", CurrentDir, "index.bleve")
	index, _ := bleve.Open(name)
	query := bleve.NewQueryStringQuery(term)
	searchRequest := bleve.NewSearchRequest(query)
	searchResult, _ := index.Search(searchRequest)
	result := []string{}
	for _, hit := range searchResult.Hits {
		result = append(result, strings.ReplaceAll(hit.ID, "/", ":"))
	}
	index.Close()
	return result
}
