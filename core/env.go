package core

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/micro/go-config"
	"github.com/micro/go-config/source/file"
)

// AndroidMode - true if Android application
var AndroidMode = false

// CurrentDir - current working directory
var CurrentDir string

// CFGFILENAME - name of the config file.
const CFGFILENAME = "gokuwiki.json"

// SYSDIRS - system folders
var SYSDIRS = []string{"data/pages", "data/media"}

// CreateDefaultConfig - prepare default config file.
func CreateDefaultConfig(filename string) {
	AppConfig = DefaultConfig
	SaveConfig()
}

// SaveConfig - save configuration to file
func SaveConfig() {
	jsonData, err := json.MarshalIndent(AppConfig, "", " ")
	if err != nil {
		panic(err)
	}
	cfgname := fmt.Sprintf("%v%v%v", CurrentDir, string(os.PathSeparator), CFGFILENAME)
	ioutil.WriteFile(cfgname, jsonData, os.ModePerm)
}

// CreateDirs - check if all directories exist, if not, create them.
func CreateDirs() {
	for _, dir := range SYSDIRS {
		dr := fmt.Sprintf("%v%v%v", CurrentDir, string(os.PathSeparator), dir)
		if _, err := os.Stat(dr); os.IsNotExist(err) {
			os.MkdirAll(dr, os.ModePerm)
		}
	}
}

// LoadConfig - load configuration
func LoadConfig() {
	if AndroidMode {
		CurrentDir = "/sdcard/gokuwiki"
	} else {
		cdir, err := os.Getwd()
		if err != nil {
			log.Printf("%v", err)
		}
		CurrentDir = cdir
	}
	if _, err := os.Stat(CurrentDir); os.IsNotExist(err) {
		os.MkdirAll(CurrentDir, os.ModePerm)
	}
	cfgname := fmt.Sprintf("%v%v%v", CurrentDir, string(os.PathSeparator), CFGFILENAME)
	if !FileExists(cfgname) {
		CreateDefaultConfig(cfgname)
	}
	config.Load(file.NewSource(
		file.WithPath(cfgname)))
	config.Scan(&AppConfig)
}

// ANDROID STUFF

// Imports
// "golang.org/x/mobile/app"
// "golang.org/x/mobile/event/lifecycle"
// "golang.org/x/mobile/event/paint"
// "golang.org/x/mobile/event/size"
// "golang.org/x/mobile/event/touch"
// "golang.org/x/mobile/exp/app/debug"
// "golang.org/x/mobile/exp/gl/glutil"
// "golang.org/x/mobile/gl"

// var (
//	determined = make(chan struct{})
//	ok         = false
// )

// var (
// 	images   *glutil.Images
// 	fps      *debug.FPS
// 	program  gl.Program
// 	position gl.Attrib
// 	offset   gl.Uniform
// 	color    gl.Uniform
// 	buf      gl.Buffer

// 	green  float32
// 	touchX float32
// 	touchY float32
// )

// AndroidRun - run app on Android platform
// func AndroidRun() {
// 	app.Main(func(a app.App) {
// 		var glctx gl.Context
// 		var sz size.Event
// 		StartServer()
// 		for e := range a.Events() {
// 			switch e := a.Filter(e).(type) {
// 			case lifecycle.Event:
// 				switch e.Crosses(lifecycle.StageVisible) {
// 				case lifecycle.CrossOn:
// 					glctx, _ = e.DrawContext.(gl.Context)
// 					a.Send(paint.Event{})
// 				case lifecycle.CrossOff:
// 					glctx = nil
// 				}
// 			case size.Event:
// 				sz = e
// 				touchX = float32(sz.WidthPx / 2)
// 				touchY = float32(sz.HeightPx / 2)
// 			case paint.Event:
// 				if glctx == nil || e.External {
// 					// As we are actively painting as fast as
// 					// we can (usually 60 FPS), skip any paint
// 					// events sent by the system.
// 					continue
// 				}

// 				a.Publish()
// 				// Drive the animation by preparing to paint the next frame
// 				// after this one is shown.
// 				a.Send(paint.Event{})
// 			case touch.Event:
// 				touchX = e.X
// 				touchY = e.Y
// 			}
// 		}
// 	})
// }
