package core

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/h2non/filetype"
)

// Last - get last element from slice
func Last(list []string) string {
	if len(list) == 0 {
		return ""
	}
	return list[len(list)-1]
}

// Wolast - return list with last element removed
func Wolast(list []string) []string {
	if len(list) == 0 {
		return []string{}
	}
	return list[:len(list)-1]
}

// LastAtFsPath - get last element at path
func LastAtFsPath(path string) string {
	parts := strings.Split(path, string(os.PathSeparator))
	return parts[len(parts)-1]
}

// DirAtFsPath - return potential dir
func DirAtFsPath(path string) string {
	parts := strings.Split(path, string(os.PathSeparator))
	return strings.Join(parts[:len(parts)-1], string(os.PathSeparator))
}

// Ns - get page namespace
func Ns(text string) string {
	parts := strings.Split(text, ":")
	if len(parts) > 1 {
		return strings.Join(parts[:len(parts)-1], ":")
	}
	return ""
}

func FiletypeIcon(path string) (string, string) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(fmt.Errorf("%v, path %v", err, path))
	}
	info, err := file.Stat()
	if err != nil {
		log.Fatal(fmt.Errorf("%v, path %v", err, path))
	}
	head := make([]byte, 261)
	file.Read(head)
	file.Close()
	tp, _ := filetype.Get(head)

	switch {
	case info.IsDir():
		return "fas fa-folder", "application/folder"
	case tp.MIME.Value == "application/pdf":
		return "fas fa-file-pdf", tp.MIME.Value
	case filetype.IsImage(head):
		return "fas fa-file-image", tp.MIME.Value
	case filetype.IsArchive(head):
		return "fas fa-file-archive", tp.MIME.Value
	case filetype.IsDocument(head):
		return "fas fa-file-alt", tp.MIME.Value
	default:
		return "fas fa-file", tp.MIME.Value
	}
}

type EditSnippet struct {
	Name string
	Code string
}

var EditSnippets = []EditSnippet{
	EditSnippet{"Page list", "<pagelist ns=\"\" columns=\"3\" depth=\"3\" dirheader=\"b\" style=\"circle\" >"},
	EditSnippet{"Media list", "<medialist ns=\"\">"},
	EditSnippet{"Add page", "<addpage hastpl=\"false\">"},
	EditSnippet{"Image thumbnail", "<thumbnail src=\"\" width=\"\" height=\"\" >"},
	EditSnippet{"Highlight", "<hl>   </hl>"},
	EditSnippet{"Info badge", "<inf>   </inf>"},
	EditSnippet{"Popover", "<popover focus=\"true\" title=\"\" btn-class=\"\">\n\n</popover>"},
	EditSnippet{"Include", "<include path=\"\">"},
	EditSnippet{"Gallery", "<gallery ns=\"\" width=\"\" height=\"\" maxrow=\"\" margin=\"\" >"},
	EditSnippet{"Fold", "<fold title=\"\" btn-class=\"\">\n\n</fold>"},
}

// FindFirstHeader - find first header in markdown file
func FindFirstHeader(content string) string {
	reg := regexp.MustCompile(`(?m)^\s*([#]+)\s+(.*)\n`)
	indexes := reg.FindAllStringSubmatchIndex(content, -1)
	if len(indexes) > 0 {
		j := indexes[0]
		header := content[j[4]:j[5]]
		return header
	}
	return ""
}
