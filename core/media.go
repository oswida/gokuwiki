package core

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
)

// MediaInfo - media info structure
type MediaInfo struct {
	Name   string
	Path   string
	FsPath string
	IsDir  bool
	Size   string
	Icon   string
}

const MediaRootTitle = "Root"

// ListMedia - list media files
func ListMedia(dir string) []MediaInfo {
	result := []MediaInfo{}
	var path string
	if dir != "" {
		path = GetMediaPath() + string(os.PathSeparator) +
			strings.ReplaceAll(dir, ":", string(os.PathSeparator))
	} else {
		path = GetMediaPath()
	}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	for _, fl := range files {
		uri := dir + ":" + fl.Name()
		for strings.HasPrefix(uri, ":") {
			uri = strings.Replace(uri, ":", "", 1)
		}
		fspath := path + string(os.PathSeparator) + fl.Name()
		icon, _ := FiletypeIcon(fspath)
		if fl.IsDir() {
			child := MediaInfo{fl.Name(), uri, fspath, true, fmt.Sprintf("%.2f", float32(fl.Size())/1024.0), icon}
			result = append(result, child)
		} else {
			page := MediaInfo{fl.Name(), uri, fspath, false, fmt.Sprintf("%.2f", float32(fl.Size())/1024.0), icon}
			result = append(result, page)
		}
	}
	sort.Slice(result, func(i, j int) bool {
		a := result[i]
		b := result[j]
		switch {
		case a.IsDir && !b.IsDir:
			return true
		case !a.IsDir && b.IsDir:
			return false
		default:
			return a.Name < b.Name
		}
	})
	return result
}

// CreateMediaFolder - create media folder
func CreateMediaFolder(path string) {
	pth := GetMediaPath() + string(os.PathSeparator) + strings.ReplaceAll(path, ":", string(os.PathSeparator))
	if !FileExists(pth) {
		os.MkdirAll(pth, os.ModePerm)
	}
}

// DeleteMediaFolder - create media folder
func DeleteMediaFolder(path string) {
	pth := GetMediaPath() + string(os.PathSeparator) + strings.ReplaceAll(path, ":", string(os.PathSeparator))
	if FileExists(pth) {
		os.RemoveAll(pth)
	}
}
