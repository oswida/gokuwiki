package core

// CfgDirs - directories
type CfgDirs struct {
	Data      string
	Media     string
	Templates string
}

// CfgGlobal - global settings
type CfgGlobal struct {
	NsIndex       string
	BaseFontSize  string
	TemplateNs    string
	UiPort        int
	LaunchBrowser bool
}

// CfgEditor - editor
type CfgEditor struct {
	ShowLineNumbers bool
	WrapLines       bool
}

// CfgType - configuration
type CfgType struct {
	Dirs    CfgDirs
	Globals CfgGlobal
	Editor  CfgEditor
}

// AppConfig - application configuration
var AppConfig CfgType

// DefaultConfig - default configuration
var DefaultConfig = CfgType{
	Dirs:    CfgDirs{"data/pages", "data/media", "templates"},
	Globals: CfgGlobal{"start", "16", "tpl", 8080, true},
	Editor:  CfgEditor{false, false},
}
